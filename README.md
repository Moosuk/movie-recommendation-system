This is a Movie Recommendation System using Collaborative Filtering.
The programme will take data file and number of samples user wants to take
and calculates Mean Absolute Error of n samples by finding difference of
actual rating and estimated rating.

by Moosuk Pyun

This program is suitable for python3.5 and above,
And must install 3 libraries:
    - numpy for matrix interpolation and calculation
    - scipy for cosine distance calculation
    - sklearn for calculating k-NearestNeighbors

Options

-f: User must input name of file that contains the data of rating, this data must be
    in userId,movieID,rating format for all .csv file. For all other file type, the data
    must be userID movieId rating (seperated by one white space).

-n: User can specify the number of samples to take, it will take n random samples from the data.
    By default, number of sample is set to 100

-b: This parameter will calculate the baseline mean absolute error, the baseline is calculated by
    calculating the average ratings of sample movie. By default, this is turned off

Example

    `python3.5 recommend.py -f u.data -n 1000`
    - This example will input u.data file and produce mean absolute error of 1000 samples

    `python3.5 recommend.py -f u.data -n 1000 -b`
    - This example will input u.data file and produce baseline mean absolute error of 1000 samples

Output Data

In testdata directory, there are 5 output test data. Each output data shows how the author implemented system to reduce the mean absolute error and the calculation time of the program. These data were generated from u.data retrieved from 100K movieLen data, it consists of 100000 ratings by 943 users on 1682 items.

    - output0.test: shows baseline mean absolute error for comparison
    - output1.test: shows MAE and run time when the program used user - user collaborative filtering
    - output2.test: shows MAE and run time when the program used item - item collaborative filtering
    - output3.test: shows MAE and run time when the program used normalized item - item collaborative filtering
    - output4.test: show MAE and run time when the program used pre-computed normalized matrix and k-NN of movies to calculate normalized item - item collaborative filtering


