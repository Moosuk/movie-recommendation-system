import sys
import getopt
import numpy as np
import codecs
import re
import random
import time
from sklearn.neighbors import LSHForest
from collabFilter import *

def main():
    filename = ''
    num_sample = 100
    cmd_options = "f:n:b"
    start_time = time.time()
    baseline = False

    try:
        options, args = getopt.getopt(sys.argv[1:], cmd_options)
    except getopt.GetoptError as err:
        print("Error:", err)

    for opt, arg in options:
        if opt == "-f":
            filename = arg
        elif opt == "-n":
            num_sample = int(arg)
        elif opt == "-b":
            baseline = True

    print("Movie Recommendation System")
    print("Input file: " + filename)
    print("Number of samples: " + str(num_sample))
    npMatrix, sample = getMatrix(filename, num_sample)
    # print('Finished inputting data')
    if baseline:
        getBaseline(npMatrix, sample)
    else:
        cf = CollaborativeFilter(npMatrix, sample)
        cf.getMAE()
    print("Programming Duration: %s seconds" % (time.time() - start_time))

# Obtain numpy array and sample array from input data
def getMatrix(filename, num_sample):
    num_users, num_movies, num_data = getNumDetail(filename)
    matrix = np.zeros((num_movies, num_users))
    randSample = random.sample(range(0, num_data), num_sample)
    sample = []
    if filename:
        try:
            fd = codecs.open(filename, 'r', encoding='utf-8')
        except Exception as err:
            print("Could not read data file")
            raise Exception("The program will stop.")

        line = fd.readline()
        count = 0
        while line:
            if re.match(r'^\D.*', line):
                line = fd.readline()
                continue
            if re.match(r'.*csv$', filename):
                element = line.split(",")
            else:
                element = line.split()

            user = int(element[0]) - 1
            movie = int(element[1]) - 1
            rating = float(element[2])

            if count == 0:
                sample_arr = [user, movie, rating]
            if count in randSample:
                arr = [user, movie, rating]
                sample.append(arr)
            matrix[movie][user] = rating
            line = fd.readline()
            count += 1
        fd.close()
        return matrix, sample

# Obtain Number of user, Number of movies and Number of data
def getNumDetail(filename):
    if filename:
        try:
            fd = codecs.open(filename, 'r', encoding='utf-8')
        except Exception as err:
            print("Could not read data file")
            raise Exception("The program will stop.")

        num_user = 0
        num_movie = 0
        num_data = 0
        line = fd.readline()
        while line:
            if re.match(r'^\D.*', line):
                line = fd.readline()
                continue
            if re.match(r'.*csv$', filename):
                element = line.split(",")
            else:
                element = line.split()

            if int(element[0]) > num_user:
                num_user = int(element[0])
            if int(element[1]) > num_movie:
                num_movie = int(element[1])

            line = fd.readline()
            num_data += 1

        fd.close()
        return num_user, num_movie, num_data

# Obtain baseline rating by averaging ratings of a movie
def getBaseline(matrix, sample_arr):
    MAE_arr = []
    for sample in sample_arr:
        user = sample[0]
        movie = sample[1]
        rating = sample[2]
        copyMatrix = np.copy(matrix)
        copyMatrix[movie][user] = 0
        estRating = np.mean(copyMatrix[movie])
        if rating > estRating:
            error = rating - estRating
        else:
            error = estRating - rating

        MAE_arr.append(error)

    print("Mean Absolute Error: " + str(np.mean(MAE_arr)))


if __name__ == "__main__":
    main()


