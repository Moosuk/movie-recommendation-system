import numpy as np
from sklearn.neighbors import LSHForest
from scipy import spatial

class CollaborativeFilter(object):

    def __init__(self, matrix, sample_arr):
        self.Matrix = matrix
        self.Sample = sample_arr

        # Pre compute normalized Matrix
        copyMatrix = np.copy(self.Matrix)
        num_movie = self.Matrix.shape[0]
        for index in range(0, num_movie - 1):
            for value in range(len(copyMatrix[index]) - 1):
                if copyMatrix[index][value] > 0:
                    copyMatrix[index][value] = copyMatrix[index][value] - np.mean(copyMatrix[index])
        self.normalMatrix = copyMatrix

        # Pre compute nearest neighbor using LSH
        lshf = LSHForest()
        lshf.fit(matrix)
        distance, indices = lshf.kneighbors(matrix, n_neighbors=16)
        self.bucket = indices

    # Obtain Mean Absolute Error
    def getMAE(self):
        MAE_arr = []
        for sampleRating in self.Sample:
            user = sampleRating[0]
            movie = sampleRating[1]
            rating = sampleRating[2]
            copyMatrix = np.copy(self.Matrix)
            copyMatrix[movie][user] = 0
            compareMatrix = []
            for rates in copyMatrix[movie]:
                compareMatrix.append(rates)

            mean = np.mean(copyMatrix[movie])

            for index in range(len(compareMatrix)-1):
                if compareMatrix[index] > 0:
                    compareMatrix[index] = compareMatrix[index] - mean


            sim_top20 = [[-2, 0]] * 15
            num_movie = self.Matrix.shape[0]
            for index in self.bucket[movie]:
                if (self.Matrix[index][user] != 0 and index != movie):
                    similarity = 1 - spatial.distance.cosine(compareMatrix, self.normalMatrix[index])
                    for i in range(0, 14):
                        if sim_top20[i][0] < similarity:
                            sim_top20[i] = [similarity, index]
                            break

            # print(sim_matrix)
            cumm_rating = 0
            cumm_sim = 0
            for index in range(0, 14):
                if sim_top20[index][0] != -2:
                    cumm_sim += sim_top20[index][0]
                    cumm_rating = cumm_rating + sim_top20[index][0] * self.Matrix[sim_top20[index][1]][user]

            if cumm_sim != 0:
                estRating = cumm_rating/cumm_sim
                if estRating > rating:
                    error = estRating - rating
                else:
                    error = rating - estRating

                MAE_arr.append(error)

        print("Mean Absolute Error: " + str(np.mean(MAE_arr)))
